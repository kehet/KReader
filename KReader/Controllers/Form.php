<?php

/**
 * Form controller
 *
 * @author Kehet
 */

namespace KReader\Controllers;

class Form extends \KReader\Controller
{

    public function form()
    {
        $categories = \CategoryQuery::create()->find();

        if (isset($_POST['submit'])) {
            $url = $_POST['url'];
            $categoryId = $_POST['category'];

            $category = \CategoryQuery::create()->findOneById($categoryId);

            if ($category == null) {
                $this->renderPage(
                    'form',
                    array(
                        'error' => 'Category not found!',
                        'categories' => $categories,
                    )
                );
                return;
            }

            $feed = new \SimplePie();
            $feed->set_feed_url($url);
            $feed->set_cache_location(URI . '.simplepie-cache');
            $feed->init();

            $site = \SiteQuery::create()->findOneByFeedUrl($feed->subscribe_url());

            if ($site != null) {
                $this->renderPage(
                    'form',
                    array(
                        'error' => 'Feed already exist!',
                        'categories' => $categories,
                    )
                );
                return;
            }

            $site = new \Site();
            $site->setCategory($category)
                ->setTitle($feed->get_title())
                ->setSiteUrl($feed->get_permalink())
                ->setFeedUrl($feed->subscribe_url())
                ->save();

            $this->renderPage(
                'form',
                array(
                    'success' => 'Feed "' . $feed->get_title() . '" added',
                    'categories' => $categories,
                )
            );
            return;
        }
        $this->renderPage(
            'form',
            array(
                'categories' => $categories,
            )
        );
    }

}