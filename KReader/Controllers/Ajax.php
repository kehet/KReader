<?php

/**
 * Ajax controller
 *
 * @author Kehet
 */

namespace KReader\Controllers;


class Ajax extends \KReader\Controller
{

    /**
     * Toggle is_read status null and timestamp
     */
    public function toggleRead()
    {
        $itemId = $_POST['id'];

        try {
            $item = \ItemQuery::create()->findOneById($itemId);

            if ($item == null) {
                throw new \Exception('Item not found!');
            }

            if ($item->getIsRead() == null) {
                $item->setIsRead(time());
                $state = 'read';
            } else {
                $item->setIsRead(null);
                $state = 'unread';
            }

            $item->save();

            echo json_encode(
                array(
                    'status' => 'ok',
                    'state' => $state,
                )
            );
        } catch (\Exception  $e) {
            echo json_encode(
                array(
                    'status' => 'nok',
                    'message' => $e->getMessage(),
                )
            );
        }
    }
}