<?php

/**
 * Main controller
 *
 * @author Kehet
 */

namespace KReader\Controllers;

class Main extends \KReader\Controller
{

    public function latest($page = 1)
    {
        $perPage = 10;

        $items = \ItemQuery::create()
            ->orderByTimestamp('DESC')
            ->where('Item.is_read is null')
            ->joinWith('Item.Site')
            ->joinWith('Site.Category')
            ->paginate($page, $perPage);

        $count = \ItemQuery::create()->count();

        $this->renderPage(
            'latest',
            array(
                'items' => $items,
                'paginate' => array(
                    'page' => $page,
                    'total' => $count,
                    'limit' => $perPage,
                ),
            )
        );
    }

    public function listAll($page = 1)
    {
        $perPage = 10;

        $items = \ItemQuery::create()
            ->orderByTimestamp('DESC')
            ->joinWith('Item.Site')
            ->joinWith('Site.Category')
            ->paginate($page, $perPage);

        $count = \ItemQuery::create()->count();

        $this->renderPage(
            'all',
            array(
                'items' => $items,
                'paginate' => array(
                    'page' => $page,
                    'total' => $count,
                    'limit' => $perPage,
                ),
            )
        );
    }

    public function next()
    {
        $item = \ItemQuery::create()
            ->where('Item.is_read is null')
            ->orderByTimestamp('ASC')
            ->findOne();

        if ($item == null) {
            header('Location: ' . URL);
            die();
        }

        $item->setIsRead(time());
        $item->save();

        header('Location: ' . $item->getItemUrl());
        die('<a href="' . $item->getItemUrl() . '">' . $item->getTitle() . '</a>');
    }

    public function errorNotFound()
    {
        $this->renderPage('error', array(), 404);
    }

    // TODO: REMOVE ME
    public function dev()
    {
        $this->renderPage('dev');
    }

}