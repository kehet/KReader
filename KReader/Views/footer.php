<footer class="row" role="contentinfo">
    <div class="large-12">
        <hr/>
        <p>
            &copy; <a href="https://github.com/Kehet/KReader">K-Reader</a>
            made by <a href="http://kehet.com/">Kehet</a> & licenced under MIT -licence.
            Newsfeed rights belong to their respective owners.
        </p>
    </div>
</footer>

<script>
    URL = "<?= URL ?>";
</script>

<script src="<?= URL ?>assets/js/vendor/jquery.js"></script>

<script src="<?= URL ?>assets/js/foundation/foundation.js"></script>
<script src="<?= URL ?>assets/js/foundation/foundation.forms.js"></script>
<script src="<?= URL ?>assets/js/foundation/foundation.alerts.js"></script>

<script src="<?= URL ?>assets/js/vendor/jquery.infinitescroll.js"></script>
<script src="<?= URL ?>assets/js/script.js"></script>

<script>
    $(document).foundation();
</script>

</body>
</html>