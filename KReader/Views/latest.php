<div class="row" role="main">
    <div class="large-12 columns" id="js-infiniteScroll">
        <h2>Latest articles</h2>

        <?php if ($items->isEmpty() OR $items->getPage() > $items->getLastPage()): ?>
            <div class="alert-box alert">
                No new unread articles found!
            </div>
        <?php else: ?>

            <?php foreach ($items as $key => $item) : ?>
                <?php $site = $item->getSite(); ?>
                <article class="feedItem js-itemsHook">
                    <header>
                        <h2><a href="<?= $item->getItemUrl() ?>"><?= $item->getTitle() ?></a>
                            <small><?= $site->getCategory()->getName() ?></small>
                        </h2>
                    </header>

                    <?= $item->getContent() ?>

                    <footer>
                        <a href="<?= $site->getSiteUrl() ?>"><?= $site->getTitle() ?></a> /
                        about
                        <span title="<?= date('c', $item->getTimestamp()) ?>">
                            <?= secs2str(time() - $item->getTimestamp()) ?>
                        </span>
                        ago /
                        <?php if ($item->getIsRead() == null) : ?>
                            <a href="#" class="js-toggleRead js-hide" data-id="<?= $item->getId() ?>">mark as read</a>
                        <?php endif; ?>
                    </footer>
                    <hr/>
                </article>
            <?php endforeach; ?>

            <?php if ($items->haveToPaginate()): ?>
                <div class="pagination-centered">
                    <?= paginate($items->getPage(), $items->getLastPage(), URL . 'latest/') ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>