<form action="<?= URL ?>add/" method="post" class="custom">
    <div class="row">
        <div class="large-8 push-2 columns">
            <div class="row collapse">
                <div class="small-6 columns">
                    <input type="text" name="url" placeholder="Site URL"/>
                </div>
                <div class="small-4 columns">
                    <select name="category" class="medium">
                        <?php foreach ($categories as $category) : ?>
                            <option value="<?= $category->getId() ?>"><?= $category->getName() ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="small-2 columns">
                    <input type="submit" name="submit" value="Send" class="button prefix"/>
                </div>
            </div>
        </div>
    </div>
</form>