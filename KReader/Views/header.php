<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>K-Reader</title>

    <link rel="stylesheet" href="<?= URL ?>assets/css/normalize.css"/>
    <link rel="stylesheet" href="<?= URL ?>assets/css/app.css"/>
    <script src="<?= URL ?>assets/js/vendor/custom.modernizr.js"></script>
</head>
<body>

<header class="row">
    <div class="large-12">
        <h1>
            <a href="<?= URL ?>">K-Reader</a>
            <small>Self hosted newsfeed reader made easy</small>
        </h1>
    </div>
</header>

<nav class="row" role="navigation">
    <div class="large-12">
        <ul class="button-group">
            <li><a href="<?= URL ?>">Latest</a></li>
            <li><a href="<?= URL ?>all/">All</a></li>
            <li><a href="<?= URL ?>next/">Next</a></li>
            <li><a href="<?= URL ?>add/">Add feed</a></li>
        </ul>
        <hr/>
    </div>
</nav>

<?php if (isset($success)) : ?>
    <div class="row">
        <div data-alert class="large-12 alert-box success">
            <?= $success ?>
            <a href="#" class="close">&times;</a>
        </div>
    </div>
<?php endif; ?>
<?php if (isset($error)) : ?>
    <div class="row">
        <div data-alert class="large-12 alert-box alert">
            <?= $error ?>
            <a href="#" class="close">&times;</a>
        </div>
    </div>
<?php endif; ?>

