<?php

/**
 * Base controller
 *
 * @author Kehet
 */

namespace KReader;


class Controller extends \Slim\Slim
{

    protected $data;

    /**
     * Construct \Slim\Slim and import some settings from settings.php
     */
    public function __construct()
    {
        $settings = require '../settings.php';
        if (isset($settings['model'])) {
            $this->data = $settings['model'];
        }
        parent::__construct($settings);
    }

    /**
     * Override to allow using views without .php
     *
     * @param string $name The name of the template passed into the view's render() method
     * @param array $data Associative array of data made available to the view
     * @param int $status The HTTP response status code to use (optional)
     */
    public function render($name, $data = array(), $status = null)
    {
        if (strpos($name, '.php') === false) {
            $name = $name . '.php';
        }
        parent::render($name, $data, $status);
    }

    /**
     * Insert header and footer to render
     *
     * @param string $name The name of the template passed into the view's render() method
     * @param array $data Associative array of data made available to the view
     * @param int $status The HTTP response status code to use (optional)
     */
    public function renderPage($name, $data = array(), $status = null)
    {
        $this->render('header', $data);
        $this->render($name, $data, $status);
        $this->render('footer');
    }

}