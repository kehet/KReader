
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- item
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `item`;

CREATE TABLE `item`
(
    `id` VARCHAR(32) NOT NULL,
    `site_id` INTEGER NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `content` TEXT NOT NULL,
    `timestamp` INTEGER NOT NULL,
    `item_url` VARCHAR(255) NOT NULL,
    `is_read` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `item_FI_1` (`site_id`),
    CONSTRAINT `item_FK_1`
        FOREIGN KEY (`site_id`)
        REFERENCES `site` (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8';

-- ---------------------------------------------------------------------
-- site
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `site`;

CREATE TABLE `site`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `category_id` INTEGER NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `site_url` VARCHAR(255) NOT NULL,
    `feed_url` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `site_FI_1` (`category_id`),
    CONSTRAINT `site_FK_1`
        FOREIGN KEY (`category_id`)
        REFERENCES `category` (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8';

-- ---------------------------------------------------------------------
-- category
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8';

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
