KReader (beta)
==============

installation
------------
1. Install components using composer. [Guide to install and use composer](http://net.tutsplus.com/tutorials/php/easy-package-management-with-composer/).

run command ```composer install```

2. Edit database credentials if needed. [Guide to install propel-gen](http://propelorm.org/documentation/01-installation.html).

edit file ```build.properties```

edit file ```runtime-conf.xml```

run command ```propel-gen convert-conf```

run command ```propel-gen insert-sql```

3. Install new crontab like this

```*/5 * * * * /usr/bin/php /path/to/kreader/crontab.php >> /path/to/logs/crontab.log```

requirements
------------
* PHP 5.3.0 or newer, with the DOM (libxml2) module enabled
* any PDO -database
* composer
* propel-gen

made with love using
--------------------
* [composer](http://getcomposer.org/)
* [slim](http://www.slimframework.com/)
* [propel](http://propelorm.org/)
* [simplepie](http://simplepie.org/)
* [compass](http://compass-style.org/) / [sass](http://sass-lang.com/)
* [phpstorm](http://www.jetbrains.com/phpstorm/)
