<?php

/**
 * index.php aka our bootstrap
 *
 * @author Kehet
 */

define('DS', DIRECTORY_SEPARATOR);
define('URI', realpath('..') . DS);
define('URL', 'http://localhost/kreader/');

// check if uri exist
if (!is_dir(URI)) {
    header('HTTP/1.1 503 Service Unavailable.', true, 503);
    die('System was unable to define root URI');
}

// composer autoloader
require URI . 'vendor' . DS . 'autoload.php';


Propel::init(URI . 'build' . DS . 'conf' . DS . 'kreader-conf.php');
set_include_path(URI . 'build' . DS . 'classes' . PATH_SEPARATOR . get_include_path());

$router = new \KReader\Router;

$routes = array(
    '/' => 'Main:latest',
    '/latest/' => 'Main:latest',
    '/latest/:page' => 'Main:latest',
    '/all/' => 'Main:listAll',
    '/all/:page' => 'Main:listAll',
    '/next/' => 'Main:next',
    '/add/' => 'Form:form',
    '/ajax/toggleRead/' => 'Ajax:toggleRead',
    '/dev/' => 'Main:dev', // TODO: REMOVE ME
);

$router->addRoutes($routes);
$router->set404Handler('Main:errorNotFound');
$router->run();


// TODO: move these to somewhere
function secs2str($secs)
{
    if ($secs >= 60 * 60 * 24) {
        $days = floor($secs / (60 * 60 * 24));

        return $days . ' day' . ($days != 1 ? 's' : '');
    }

    if ($secs >= 60 * 60) {
        $hours = floor($secs / (60 * 60));

        return $hours . ' hour' . ($hours != 1 ? 's' : '');
    }

    if ($secs >= 60) {
        $minutes = floor($secs / 60);

        return $minutes . ' minute' . ($minutes != 1 ? 's' : '');
    }

    return $secs . ' second' . ($secs != 1 ? 's' : '');
}

function paginate($page = 1, $lastPage = 1, $urlPrefix = '')
{
    $stages = 3;

    $out = '';
    if ($lastPage < 1) {
        return $out;
    }

    $out .= '<ul class="pagination">' . PHP_EOL;

    if ($page > 1) {
        $out .= '    <li class="arrow"><a href="' . $urlPrefix . ($page - 1) . '">&laquo;</a></li>' . PHP_EOL;
    } else {
        $out .= '    <li class="arrow unavailable"><a href="#">&laquo;</a></li>' . PHP_EOL;
    }

    if ($lastPage < 7 + ($stages * 2)) { // Not enough pages to breaking it up
        for ($i = 1; $i <= $lastPage; $i++) {
            if ($i == $page) {
                $out .= '    <li class="current"><a href="' . $urlPrefix . $i . '">' . ($i) . '</a></li>' . PHP_EOL;
            } else {
                $out .= '    <li><a href="' . $urlPrefix . $i . '">' . ($i) . '</a></li>' . PHP_EOL;
            }
        }
    } elseif ($lastPage > 5 + ($stages * 2)) { // Enough pages to hide a few?
        // Beginning only hide later pages
        if ($page < 1 + ($stages * 2)) {
            for ($i = 1; $i <= 4 + ($stages * 2); $i++) {
                if ($i == $page) {
                    $out .= '    <li class="current"><a href="' . $urlPrefix . $i . '">' . ($i) . '</a></li>' . PHP_EOL;
                } else {
                    $out .= '    <li><a href="' . $urlPrefix . $i . '">' . ($i) . '</a></li>' . PHP_EOL;
                }
            }
            $out .= '    <li class="unavailable"><a href="#">...</a></li>' . PHP_EOL;
            $out .= '    <li><a href="' . $urlPrefix . ($lastPage - 1) . '">' . ($lastPage - 1) . '</a></li>' . PHP_EOL;
            $out .= '    <li><a href="' . $urlPrefix . $lastPage . '">' . $lastPage . '</a></li>' . PHP_EOL;
        } // Middle hide some front and some back
        elseif ($lastPage - ($stages * 2) > $page AND $page > ($stages * 2)) {
            $out .= '    <li><a href="' . $urlPrefix . '1">1</a></li>' . PHP_EOL;
            $out .= '    <li><a href="' . $urlPrefix . '2">2</a></li>' . PHP_EOL;
            $out .= '    <li class="unavailable"><a href="#">...</a></li>' . PHP_EOL;

            for ($i = $page - $stages; $i <= $page + $stages; $i++) {
                if ($i == $page) {
                    $out .= '    <li class="current"><a href="' . $urlPrefix . $i . '">' . ($i) . '</a></li>' . PHP_EOL;
                } else {
                    $out .= '    <li><a href="' . $urlPrefix . $i . '">' . ($i) . '</a></li>' . PHP_EOL;
                }
            }

            $out .= '    <li class="unavailable"><a href="#">...</a></li>' . PHP_EOL;
            $out .= '    <li><a href="' . $urlPrefix . ($lastPage - 1) . '">' . ($lastPage - 1) . '</a></li>' . PHP_EOL;
            $out .= '    <li><a href="' . $urlPrefix . $lastPage . '">' . ($lastPage) . '</a></li>' . PHP_EOL;
        } else { // End only hide early pages
            $out .= '    <li><a href="' . $urlPrefix . '1">1</a></li>' . PHP_EOL;
            $out .= '    <li><a href="' . $urlPrefix . '2">2</a></li>' . PHP_EOL;
            $out .= '    <li class="unavailable"><a href="#">...</a></li>' . PHP_EOL;

            for ($i = $lastPage - (3 + ($stages * 2)); $i <= $lastPage; $i++) {
                if ($i == $page) {
                    $out .= '    <li class="current"><a href="' . $urlPrefix . $i . '">' . ($i) . '</a></li>' . PHP_EOL;
                } else {
                    $out .= '    <li><a href="' . $urlPrefix . $i . '">' . ($i) . '</a></li>' . PHP_EOL;
                }
            }
        }
    }

    // Next
    if ($page < $lastPage) {
        $out .= '    <li class="arrow"><a href="' . $urlPrefix . ($page + 1) . '" id="js-nextHook">&raquo;</a></li>' . PHP_EOL;
    } else {
        $out .= '    <li class="arrow unavailable"><a href="#">&raquo;</a></li>' . PHP_EOL;
    }

    $out .= '</ul>' . PHP_EOL;


    return $out;
}

