$(document).ready(function() {
    $('#js-infiniteScroll').infinitescroll({
        nextSelector: "ul.pagination a#js-nextHook",
        itemSelector: "article.js-itemsHook",
        navSelector: "ul.pagination",
        dataType: 'html',
        bufferPx: 250
    });
    $('.js-toggleRead').click(function(e) {
        e.preventDefault();

        var that = $(this);

        $.post(URL + 'ajax/toggleRead/', {
            'id': that.data('id')
        }, function(data) {
            if(data.status == "ok") {
                if(data.state == "read") {
                    that.html("mark as unread");
                } else {
                    that.html("mark as read");
                }

                if(that.hasClass('js-hide')) {
                    that.parent().parent().slideUp();
                }
            } else {
                alert("Error: " + data.message);
            }
        }, "json");

    });
});